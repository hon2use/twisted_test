class User:
    perm_list = ['create', 'edit', 'show', 'delete']

    def __init__(self, name, perm):
        self.name = name
        self.perm = perm
