'''
Develop Restful Service using Twisted Framework :
1. Show, Add, Edit, Delete Product List with:
1.1 Role-based authorization (access to product categories, product editing, product adding, product removal)
2. Implement Response Cache
3. Implement asynchronous sending requests for the use of the function (display of products) to the statistics server
4. Mysql User Store (hashing and checking passwords with salt)
5. Implement RateLimits
Push code to github with all commits
'''

import sqlite3
import hashlib
import logging
from functools import cached_property

from twisted.web import server, resource, http_headers
from twisted.internet import reactor, endpoints

conn = sqlite3.connect("mydatabase.db")
cursor = conn.cursor()


class Index(resource.Resource):
    isLeaf = True

    def getChild(self, name, request):
        if name == '':
            return self
        return resource.getChild(self, name, request)

    @cached_property
    def render_GET(self, request):
        if request.prepath == 'product':
            if self.has_perm(request, 'product', 'read'):  # DRY QQ
                try:
                    products = cursor.execute("SELECT * FROM products").fetchall()
                except sqlite3.DatabaseError as err:
                    print("Error: ", err)
                else:
                    return products
            else:
                return 'No permission'

        if request.prepath == 'register':
            # get cookies
            # if not login redirect
            login = False
            user_id = 1
            if login:
                try:
                    user = cursor.execute("SELECT * FROM users WHERE id=?", user_id).fetchall()
                except sqlite3.DatabaseError as err:
                    print("Error: ", err)
                else:
                    return user

    def render_POST(self, request):
        if request.prepath == 'register':
            if request.args['password'] != request.args['password2']:
                return 'Passwords do not match'

            username = request.args['username']
            pwd = request.args['password2']
            salt = hashlib.md5(pwd.encode())

            try:
                cursor.execute("INSERT INTO users VALUES (?,?)", (username, salt.hexdigest()))
            except sqlite3.DatabaseError as err:
                print("Error: ", err)
            else:
                conn.commit()
                return 'register done'
            # get cookies
            # if not login:

        if request.prepath == 'product':
            if self.has_perm(request, 'product', 'create'):
                # val = request.content.getvalue()
                val = request.args
                try:
                    cursor.execute("INSERT INTO products VALUES(?,?)",
                                   (val['name'], val['description']))
                except sqlite3.DatabaseError as err:
                    print("Error: ", err)
                else:
                    conn.commit()
                    return 'done'
            else:
                return 'No permission'
        else:
            return '404'

    def render_PUT(self, request):
        if request.prepath == 'product':
            if self.has_perm(request, 'product', 'edit'):
                # val = request.content.getvalue()
                val = request.args
                try:
                    cursor.execute("UPDATE products set values(?,?) WHERE id=?",
                                   (val['name'], val['description'], val['id']))
                except sqlite3.DatabaseError as err:
                    print("Error: ", err)
                else:
                    conn.commit()
                    return 'done'
            else:
                return 'No permission'
        else:
            return '404'

    def render_DELETE(self, request):
        if request.prepath == 'product':
            if self.has_perm(request, 'product', 'delete'):
                # val = request.content.getvalue()
                val = request.args
                return cursor.execute("DELETE FROM products WHERE id=?",
                                      (val['id'],))
            else:
                return 'No permission'
        else:
            return '404'

    @staticmethod
    def has_perm(request, entity, action, role='default'):
        u_id = request.args['user']
        # u_id = http_headers.getRawHeaders('user')
        # perm = cursor.execute("SELECT perm FROM users WHERE id=?", (u_id,)).fetchone()
        perm = cursor.execute("SELECT name FROM perms WHERE user=? and entity=?", (u_id, entity)).fetchall()
        if [p for p in perm if perm == action]:
            return True
        else:
            return False

        # todo represent in db
        permissions = {'admin': {'product': [
            'create',
            'read',
            'update',
            'delete'
        ]}}
        if permissions[role][entity]:
            if [m for m in permissions[role][entity] if action in m]:
                return True
            else:
                return False


root = resource.Resource()
root.putChild("product", Index())
root.putChild("register", Index())
# root.putChild("auth", Index())
factory = server.Site(root)
reactor.listenTCP(8880, factory)
reactor.run()
